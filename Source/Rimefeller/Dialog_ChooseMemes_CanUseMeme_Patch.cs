﻿using HarmonyLib;
using RimWorld;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    [HarmonyPatch(typeof(Dialog_ChooseMemes), "CanUseMeme")]
    static class Dialog_ChooseMemes_CanUseMeme_Patch
    {
        public static bool Postfix(bool __result, Dialog_ChooseMemes __instance, MemeDef meme, Ideo ___ideo, bool ___reformingIdeo)
        {
            if (meme.defName != "BenLubarMemesAndPrecepts_Fracking" || IdeoUIUtility.devEditMode)
            {
                return __result;
            }

            if (Faction.OfPlayerSilentFail?.def?.techLevel <= TechLevel.Medieval && (!___ideo.Fluid || !___reformingIdeo))
            {
                // neolithic and medieval player factions can't use the Fracking meme at game start,
                // but can use it when reforming a fluid ideo.
                // this is to avoid giving away "free" electricity research.

                return false;
            }

            return __result;
        }
    }
}
