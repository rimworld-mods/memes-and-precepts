﻿using System;
using Rimefeller;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    public class ThoughtWorker_Precept_Oil : ThoughtWorker_Precept
    {
        public void GetParameters(Map map, out int wells, out double oil, out double fuel)
        {
            wells = 0;
            oil = 0.0;
            fuel = map.resourceCounter.GetCount(ThingDefOf.Chemfuel);

            foreach (var wellDef in new[] { RimefellerDefOf.OilWell, RimefellerDefOf.DeepOilWell })
            {
                foreach (var well in map.listerBuildings.AllBuildingsColonistOfDef(wellDef))
                {
                    if (well.GetComp<CompOilDerrick>().IsDrilled && FlickUtility.WantsToBeOn(well))
                    {
                        wells++;
                    }
                }
            }

            foreach (var tank in map.listerBuildings.AllBuildingsColonistOfDef(RimefellerDefOf.OilStorage))
            {
                oil += tank.GetComp<CompStorageTank>().Storage;
            }

            foreach (var tank in map.listerBuildings.AllBuildingsColonistOfDef(RimefellerDefOf.FuelStorage))
            {
                fuel += tank.GetComp<CompStorageTank>().Storage;
            }
        }

        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
            if (!p.Map.IsPlayerHome)
            {
                return ThoughtState.Inactive;
            }

            var invColonists = 1.0 / Math.Max(1, p.Map.mapPawns.FreeColonistsSpawnedCount);
            GetParameters(p.Map, out var wells, out var oil, out var fuel);

            var oilPerColonist = oil * invColonists;
            var fuelPerColonist = fuel * invColonists;

            if (wells == 0 && oilPerColonist < 10.0)
            {
                // no active wells, less than 10 units of oil per colonist, any amount of chemfuel
                return ThoughtState.ActiveAtStage(0);
            }

            // otherwise, the tier is based on a weighted sum:
            return ThoughtState.ActiveAtStage(Math.Min(Math.Max((int)Math.Ceiling(wells / 3.0 + oilPerColonist / 1000.0 + fuelPerColonist / 2500.0), 1), 6));
        }

        public override string PostProcessDescription(Pawn p, string description)
        {
            var invColonists = 1.0 / Math.Max(1, p.Map.mapPawns.FreeColonistsSpawnedCount);
            GetParameters(p.Map, out var wells, out var oil, out var fuel);

            return base.PostProcessDescription(p, description) + "\n\n" +
                "BenLubarMemesAndPrecepts_ActiveOilWells".Translate() + ": " + wells + "\n" +
                "BenLubarMemesAndPrecepts_OilPerColonist".Translate() + ": " + (oil * invColonists).ToString("F1") + "\n" +
                "BenLubarMemesAndPrecepts_FuelPerColonist".Translate() + ": " + (fuel * invColonists).ToString("F1");
        }
    }
}
