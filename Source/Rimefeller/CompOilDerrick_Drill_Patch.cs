﻿using HarmonyLib;
using Rimefeller;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    [HarmonyPatch(typeof(CompOilDerrick), nameof(CompOilDerrick.Drill))]
    static class CompOilDerrick_Drill_Patch
    {
        public static void Prefix(CompOilDerrick __instance, ref float prog)
        {
            if (__instance.parent.Faction?.ideos?.PrimaryIdeo?.HasPrecept(MPDefOf.BenLubarMemesAndPrecepts_OilExtraction_Accelerated) == true)
            {
                prog *= 1.5f;
            }
        }
    }
}
