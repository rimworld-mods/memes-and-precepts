﻿using HarmonyLib;
using Rimefeller;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    [HarmonyPatch(typeof(CompOilDerrick), nameof(CompOilDerrick.BaseCapacity), MethodType.Getter)]
    static class CompOilDerrick_BaseCapacity_Patch
    {
        public static float Postfix(float __result, CompOilDerrick __instance)
        {
            if (__instance.parent.Faction?.ideos?.PrimaryIdeo?.HasPrecept(MPDefOf.BenLubarMemesAndPrecepts_OilExtraction_Accelerated) == true)
            {
                return __result * 1.25f;
            }

            return __result;
        }
    }
}
