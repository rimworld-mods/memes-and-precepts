﻿using HarmonyLib;
using Verse;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    [StaticConstructorOnStartup]
    static class RimefellerHarmony
    {
        static RimefellerHarmony()
        {
            var harmony = new Harmony("me.lubar.MemesAndPrecepts.Rimefeller");
            harmony.PatchAll();
        }
    }
}
