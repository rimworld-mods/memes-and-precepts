﻿using System.Linq;
using Rimefeller;
using RimWorld;
using Verse;
using Verse.Sound;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    public class IncidentWorker_FrackingEarthquake : IncidentWorker
    {
        protected override bool CanFireNowSub(IncidentParms parms)
        {
            var map = (Map)parms.target;
            if (map?.ParentFaction?.ideos?.PrimaryIdeo?.HasPrecept(MPDefOf.BenLubarMemesAndPrecepts_OilExtraction_Accelerated) != true)
            {
                return false;
            }

            int wells = 0;
            foreach (var net in map.Rimefeller().PipeNets)
            {
                wells += net.OilWells.Count(derrick => derrick.well?.oilField != null);
            }

            return wells > 1; // rimefeller is 6
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            var map = (Map)parms.target;
            if (!map.Rimefeller().PipeNets.SelectMany(p => p.OilWells).Where(d => d.well?.oilField != null).TryRandomElement(out var derrick))
            {
                return false;
            }

            Rand.PushState();
            derrick.well.oilField.earthquake = Rand.Range(110, 220); // rimefeller is 180 to 320
            Rand.PopState();

            DubDef.tremors.PlayOneShotOnCamera(map);
            SendStandardLetter(parms, null);

            return true;
        }
    }
}
