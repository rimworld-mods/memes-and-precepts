﻿using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts.Rimefeller
{
    [DefOf]
    public static class RimefellerDefOf
    {
        public static ThingDef OilWell;
        public static ThingDef DeepOilWell;
        public static ThingDef OilStorage;
        public static ThingDef FuelStorage;

        static RimefellerDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(RimefellerDefOf));
        }
    }
}
