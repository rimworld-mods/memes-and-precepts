﻿using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(Pawn), nameof(Pawn.DrawExtraSelectionOverlays))]
    static class Pawn_DrawExtraSelectionOverlays_Patch
    {
        public static void Postfix(Pawn __instance)
        {
            if (__instance != Find.Selector.SingleSelectedThing)
            {
                return;
            }

            var hex = (ThoughtWorker_Precept_HexagonalRooms)MPDefOf.BenLubarMemesAndPrecepts_HexagonalRooms_Expected.Worker;
            if (hex.CanHaveThought(__instance))
            {
                hex.DrawHexagon(__instance);
            }
        }
    }
}
