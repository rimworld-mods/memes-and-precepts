﻿using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class Designator_PlanHexagon_Horizontal : Designator_PlanHexagon
    {
        public Designator_PlanHexagon_Horizontal()
        {
            defaultLabel = "BenLubarMemesAndPrecepts_DesignatorPlanHexagon_Horizontal".Translate();
            icon = ContentFinder<Texture2D>.Get("UI/Designators/BenLubarMemesAndPrecepts_HexagonalDesignator_Horizontal");
        }

        public override IEnumerable<IntVec3> GetHexagon(IntVec3 min, IntVec3 max)
        {
            foreach (var cell in ThoughtWorker_Precept_HexagonalRooms.Hexagon(max.x - min.x, max.z - min.z))
            {
                yield return min + cell;
            }
        }
    }
}
