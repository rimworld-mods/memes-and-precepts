﻿using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class Designator_PlanHexagon_Vertical : Designator_PlanHexagon
    {
        public Designator_PlanHexagon_Vertical()
        {
            defaultLabel = "BenLubarMemesAndPrecepts_DesignatorPlanHexagon_Vertical".Translate();
            icon = ContentFinder<Texture2D>.Get("UI/Designators/BenLubarMemesAndPrecepts_HexagonalDesignator_Vertical");
        }

        public override IEnumerable<IntVec3> GetHexagon(IntVec3 min, IntVec3 max)
        {
            foreach (var cell in ThoughtWorker_Precept_HexagonalRooms.Hexagon(max.z - min.z, max.x - min.x))
            {
                yield return min + new IntVec3(cell.z, 0, cell.x);
            }
        }
    }
}
