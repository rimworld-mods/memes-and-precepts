﻿using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [DefOf]
    public static class MPDefOf
    {
        public static HistoryEventDef BenLubarMemesAndPrecepts_BuiltCleanPower;
        public static HistoryEventDef BenLubarMemesAndPrecepts_BuiltDirtyPower;

        public static MemeDef BenLubarMemesAndPrecepts_EcologyPrimacy;

        public static PreceptDef BenLubarMemesAndPrecepts_BedSharing_Platonic;
        public static PreceptDef BenLubarMemesAndPrecepts_BedSharing_Intimate;
        public static PreceptDef BenLubarMemesAndPrecepts_BedSharing_AfterMarriage;
        public static PreceptDef BenLubarMemesAndPrecepts_BedSharing_Despised;

        public static PreceptDef BenLubarMemesAndPrecepts_Temperature_Preferred;

        [MayRequire("Dubwise.Rimefeller")]
        public static PreceptDef BenLubarMemesAndPrecepts_OilExtraction_Accelerated;

        public static ThoughtDef BenLubarMemesAndPrecepts_HexagonalRooms_Expected;

        public static ThingDef BenLubarMemesAndPrecepts_ClimateAdjuster;
        public static ThingDef BenLubarMemesAndPrecepts_WeatherController;
        public static ThingDef BenLubarMemesAndPrecepts_SunBlocker;

        static MPDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(MPDefOf));
        }
    }
}
