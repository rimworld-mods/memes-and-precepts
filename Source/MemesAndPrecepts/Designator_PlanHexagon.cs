﻿using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [StaticConstructorOnStartup]
    public abstract class Designator_PlanHexagon : Designator_Plan
    {
        internal static readonly AccessTools.FieldRef<DesignationDragger, IntVec3> startDragCell = AccessTools.FieldRefAccess<DesignationDragger, IntVec3>("startDragCell");

        public override bool Visible
        {
            get
            {
                if (DebugSettings.godMode)
                {
                    return true;
                }

                var map = Find.CurrentMap;
                if (map == null)
                {
                    return true;
                }

                var hex = (ThoughtWorker_Precept_HexagonalRooms)MPDefOf.BenLubarMemesAndPrecepts_HexagonalRooms_Expected.Worker;
                return map.mapPawns.AllPawnsSpawned.Any(p => hex.CanHaveThought(p));
            }
        }

        public override bool DragDrawOutline => true;

        public Designator_PlanHexagon() : base(DesignateMode.Add)
        {
            defaultDesc = "BenLubarMemesAndPrecepts_DesignatorPlanHexagon_Desc".Translate();
            soundSucceeded = SoundDefOf.Designate_PlanAdd;
            hotKey = null;
        }

        private IntVec3 lastMin, lastMax;
        private HashSet<IntVec3> cachedHexagon;

        public override AcceptanceReport CanDesignateCell(IntVec3 loc)
        {
            var accepted = base.CanDesignateCell(loc);
            if (!accepted)
            {
                return accepted;
            }

            var min = startDragCell(Find.DesignatorManager.Dragger);
            var max = UI.MouseCell();

            if (min.x > max.x)
            {
                (max.x, min.x) = (min.x, max.x);
            }

            if (min.z > max.z)
            {
                (max.z, min.z) = (min.z, max.z);
            }

            if (min != lastMin || max != lastMax || cachedHexagon == null)
            {
                lastMin = min;
                lastMax = max;
                cachedHexagon = GetHexagon(min, max).ToHashSet();
            }

            return cachedHexagon.Contains(loc);
        }

        public abstract IEnumerable<IntVec3> GetHexagon(IntVec3 min, IntVec3 max);
    }
}
