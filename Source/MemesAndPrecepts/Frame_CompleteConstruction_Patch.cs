﻿using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(Frame), nameof(Frame.CompleteConstruction))]
    static class Frame_CompleteConstruction_Patch
    {
        public static void Prefix(Frame __instance, Pawn worker)
        {
            var thingDef = __instance.def.entityDefToBuild as ThingDef;
            if (thingDef == null)
            {
                return;
            }

            var clean = CompCleanPower.IsCleanPowerPlant(thingDef);
            if (clean.HasValue)
            {
                Find.HistoryEventsManager.RecordEvent(new HistoryEvent(
                    clean.Value ? MPDefOf.BenLubarMemesAndPrecepts_BuiltCleanPower : MPDefOf.BenLubarMemesAndPrecepts_BuiltDirtyPower,
                    worker.Named(HistoryEventArgsNames.Doer)
                ));
            }
        }
    }
}
