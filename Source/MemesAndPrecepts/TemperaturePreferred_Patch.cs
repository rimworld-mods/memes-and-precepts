﻿using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch]
    static class TemperaturePreferred_Patch
    {
        private static bool inThought;

        [HarmonyPrefix]
        [HarmonyPatch(typeof(ThoughtWorker_Cold), "CurrentStateInternal")]
        public static void ColdPrefix()
        {
            inThought = true;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(ThoughtWorker_Cold), "CurrentStateInternal")]
        public static void ColdPostfix()
        {
            inThought = false;
        }

        [HarmonyPrefix]
        [HarmonyPatch(typeof(ThoughtWorker_Hot), "CurrentStateInternal")]
        public static void HotPrefix()
        {
            inThought = true;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(ThoughtWorker_Hot), "CurrentStateInternal")]
        public static void HotPostfix()
        {
            inThought = false;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(StatExtension), nameof(StatExtension.GetStatValue))]
        public static float StatPostfix(float __result, Thing thing, StatDef stat)
        {
            if (!inThought)
            {
                return __result;
            }

            var pawn = thing as Pawn;
            if (pawn?.Ideo?.HasPrecept(MPDefOf.BenLubarMemesAndPrecepts_Temperature_Preferred) != true)
            {
                return __result;
            }

            if (stat == StatDefOf.ComfyTemperatureMin)
            {
                return __result + 10f;
            }

            if (stat == StatDefOf.ComfyTemperatureMax)
            {
                return __result - 10f;
            }

            return __result;
        }
    }
}
