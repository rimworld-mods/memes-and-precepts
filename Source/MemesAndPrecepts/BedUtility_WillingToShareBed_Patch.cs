﻿using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(BedUtility), nameof(BedUtility.WillingToShareBed))]
    static class BedUtility_WillingToShareBed_Patch
    {
        public static IEnumerable<PreceptDef> BedSharingPrecepts
        {
            get
            {
                yield return MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Platonic;
                yield return MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Intimate;
                yield return MPDefOf.BenLubarMemesAndPrecepts_BedSharing_AfterMarriage;
                yield return MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Despised;
            }
        }

        public static bool Postfix(bool __result, Pawn pawn1, Pawn pawn2)
        {
            if (pawn1 == pawn2)
            {
                return __result;
            }

            var bed1 = GetBedSharingPrecept(pawn1.Ideo);
            var bed2 = GetBedSharingPrecept(pawn2.Ideo);

            if (bed1 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Platonic && bed2 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Platonic)
            {
                return true;
            }

            if (bed1 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Despised || bed2 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_Despised)
            {
                return false;
            }

            if ((bed1 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_AfterMarriage || bed2 == MPDefOf.BenLubarMemesAndPrecepts_BedSharing_AfterMarriage) && !pawn1.relations.DirectRelationExists(PawnRelationDefOf.Spouse, pawn2))
            {
                return false;
            }

            return __result;
        }

        public static PreceptDef GetBedSharingPrecept(Ideo ideo)
        {
            if (ideo == null)
            {
                return null;
            }

            foreach (var preceptDef in BedSharingPrecepts)
            {
                if (ideo.HasPrecept(preceptDef))
                {
                    return preceptDef;
                }
            }

            return null;
        }
    }
}
