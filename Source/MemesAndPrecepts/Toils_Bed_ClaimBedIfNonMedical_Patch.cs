﻿using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(Toils_Bed), nameof(Toils_Bed.ClaimBedIfNonMedical))]
    static class Toils_Bed_ClaimBedIfNonMedical_Patch
    {
        public static void Postfix(Toil __result, TargetIndex ind, TargetIndex claimantIndex)
        {
            var innerAction = __result.initAction;
            __result.initAction = delegate
            {
                var actor = __result.GetActor();
                var job = actor.CurJob;
                if (job.def != JobDefOf.Lovin)
                {
                    // only patch if doing lovin'
                    innerAction();
                    return;
                }

                var pawn = ((claimantIndex == TargetIndex.None) ? actor : ((Pawn)job.GetTarget(claimantIndex).Thing));
                var bed = (Building_Bed)job.GetTarget(ind).Thing;
                if (bed.CompAssignableToPawn.IdeoligionForbids(pawn))
                {
                    // if we can't claim the bed, but we got to this point, our ideoligion forbids bed sharing but allows physical love. 
                    // simply don't claim the bed and continue as we were.
                    return;
                }

                innerAction();
            };
        }
    }
}
