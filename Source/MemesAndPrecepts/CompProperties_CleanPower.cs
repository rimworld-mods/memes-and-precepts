﻿using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class CompProperties_CleanPower : CompProperties
    {
        public bool clean = true;
        public bool notActuallyAPowerPlant;

        public CompProperties_CleanPower() : base(typeof(CompCleanPower))
        {
        }
    }
}
