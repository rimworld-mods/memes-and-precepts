﻿using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class ThoughtWorker_Precept_CleanEnergy : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
            int clean = 0, dirty = 0;

            foreach (var building in p.Map.listerBuildings.allBuildingsColonist)
            {
                var isClean = CompCleanPower.IsCleanPowerPlant(building);
                if (isClean.HasValue)
                {
                    if (isClean.Value)
                    {
                        clean++;
                    }
                    else
                    {
                        dirty++;
                    }
                }
            }

            if (clean == 0 && dirty == 0)
            {
                return ThoughtState.ActiveAtStage(0);
            }
            if (dirty == 0)
            {
                return ThoughtState.ActiveAtStage(1);
            }
            if (clean == 0)
            {
                return ThoughtState.ActiveAtStage(2);
            }
            var total = clean + dirty;
            if (clean >= total / 4 && dirty >= total / 4)
            {
                return ThoughtState.ActiveAtStage(3);
            }

            return ThoughtState.ActiveAtStage(clean > dirty ? 4 : 5);
        }
    }
}
