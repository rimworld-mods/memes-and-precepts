﻿using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(GenConstruct), nameof(GenConstruct.CanConstruct), new[] { typeof(Thing), typeof(Pawn), typeof(bool), typeof(bool) })]
    static class GenConstruct_CanConstruct_Patch
    {
        public static bool Postfix(bool __result, Thing t, Pawn p)
        {
            if (!__result)
            {
                return false;
            }

            if (!(t.def.entityDefToBuild is ThingDef thingDef))
            {
                return true;
            }

            var clean = CompCleanPower.IsCleanPowerPlant(thingDef);
            if (!clean.HasValue)
            {
                return true;
            }

            return new HistoryEvent(clean.Value ? MPDefOf.BenLubarMemesAndPrecepts_BuiltCleanPower : MPDefOf.BenLubarMemesAndPrecepts_BuiltDirtyPower, p.Named(HistoryEventArgsNames.Doer)).Notify_PawnAboutToDo_Job();
        }
    }
}
