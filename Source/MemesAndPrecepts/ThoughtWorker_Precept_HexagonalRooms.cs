﻿using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class ThoughtWorker_Precept_HexagonalRooms : ThoughtWorker_Precept
    {
        private const float Sqrt3 = 1.7320508075688772f;
        private const float HalfSqrt3 = Sqrt3 / 2f;
        private const float OnePlusHalfSqrt3 = 1f + HalfSqrt3;
        private const float OnePlusHalfSqrt3OverSqrt3 = OnePlusHalfSqrt3 / Sqrt3;
        private const float OnePlusHalfSqrt3OverSqrt3Inv = Sqrt3 / OnePlusHalfSqrt3;
        private const int MinRoomTiles = 20;

        internal bool CanHaveThought(Pawn p)
        {
            return CanHaveThought(this, p);
        }

        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
            var room = p.GetRoom();
            if (room == null || !room.ProperRoom || room.IsHuge)
            {
                return ThoughtState.Inactive;
            }

            var cells = room.Cells.ToHashSet();
            if (cells.Count < MinRoomTiles)
            {
                return ThoughtState.ActiveAtStage(21);
            }

            ComputeHexagonDiff(cells, out var horiz, out var vert);

            return ThoughtState.ActiveAtStage(Math.Max(20 - Math.Min(horiz, vert), 0));
        }

        public void ComputeHexagonDiff(HashSet<IntVec3> cells, out int horiz, out int vert)
        {
            var minX = cells.Select(c => c.x).Min();
            var maxX = cells.Select(c => c.x).Max();
            var minZ = cells.Select(c => c.z).Min();
            var maxZ = cells.Select(c => c.z).Max();
            var min = new IntVec3(minX, 0, minZ);

            var xRange = maxX - minX;
            var zRange = maxZ - minZ;

            horiz = cells.Count;
            vert = cells.Count;

            foreach (var cell in Hexagon(xRange, zRange))
            {
                if (cells.Contains(min + cell))
                {
                    horiz--;
                }
                else
                {
                    horiz++;
                }
            }

            foreach (var cell in Hexagon(zRange, xRange))
            {
                if (cells.Contains(min + new IntVec3(cell.z, 0, cell.x)))
                {
                    vert--;
                }
                else
                {
                    vert++;
                }
            }
        }

        public void DrawHexagon(Pawn pawn)
        {
            var room = pawn.GetRoom();
            if (room == null || !room.ProperRoom || room.IsHuge)
            {
                return;
            }

            var cells = room.Cells.ToHashSet();
            if (cells.Count < MinRoomTiles)
            {
                return;
            }

            ComputeHexagonDiff(cells, out var horiz, out var vert);
            var minX = cells.Select(c => c.x).Min();
            var maxX = cells.Select(c => c.x).Max();
            var minZ = cells.Select(c => c.z).Min();
            var maxZ = cells.Select(c => c.z).Max();
            var min = new IntVec3(minX, 0, minZ);

            var xRange = maxX - minX;
            var zRange = maxZ - minZ;

            var expectedCells = (vert < horiz ? Hexagon(zRange, xRange).Select(c => min + new IntVec3(c.z, 0, c.x)) : Hexagon(xRange, zRange).Select(c => min + c)).ToList();

            GenDraw.DrawFieldEdges(expectedCells);
        }

        public static IEnumerable<IntVec3> Hexagon(int xRange, int zRange)
        {
            /* r3/4   1    r3/4
             * a  b        c  d
             * e   ________
             *    /        \    r3/2
             *   /          \
             * f/            \
             * g\            /
             *   \          / slope = 2
             * h  \________/
             */

            int a, b, c, d, e, f, g, h, r3;

            a = 0;
            d = xRange;
            e = 0;
            h = zRange;

            var aspect = (float)xRange / (float)zRange;
            if (aspect < OnePlusHalfSqrt3OverSqrt3)
            {
                int expectedZ = (int)Math.Round(xRange * OnePlusHalfSqrt3OverSqrt3Inv);
                e = (zRange - expectedZ) / 2;
                h = e + expectedZ;
                r3 = expectedZ;
            }
            else
            {
                int expectedX = (int)Math.Round(zRange * OnePlusHalfSqrt3OverSqrt3);
                a = (xRange - expectedX) / 2;
                d = a + expectedX;
                r3 = zRange;
            }

            b = a + r3 / 4;
            c = d - r3 / 4;
            f = e + r3 / 2;
            g = h - r3 / 2;

            // top left
            for (int z = e; z <= f; z++)
            {
                for (int x = a + (f - z) / 2; x <= b; x++)
                {
                    yield return new IntVec3(x, 0, z);
                }
            }

            // bottom left
            for (int z = h; z >= g && z > f; z--)
            {
                for (int x = a + (z - g) / 2; x <= b; x++)
                {
                    yield return new IntVec3(x, 0, z);
                }
            }

            // top right
            for (int z = e; z <= f; z++)
            {
                for (int x = d - (f - z) / 2; x >= c && x > b; x--)
                {
                    yield return new IntVec3(x, 0, z);
                }
            }

            // bottom right
            for (int z = h; z >= g && z > f; z--)
            {
                for (int x = d - (z - g) / 2; x >= c && x > b; x--)
                {
                    yield return new IntVec3(x, 0, z);
                }
            }

            // middle
            for (int z = e; z <= h; z++)
            {
                for (int x = b + 1; x < c; x++)
                {
                    yield return new IntVec3(x, 0, z);
                }
            }
        }
    }
}
