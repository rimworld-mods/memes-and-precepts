﻿using System;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [StaticConstructorOnStartup]
    public class CompCleanPower : ThingComp
    {
        public CompProperties_CleanPower Props => (CompProperties_CleanPower)props;

        private static readonly Type CompGasPowerPlant = Type.GetType("GasNetwork.CompGasPowerPlant,VanillaPowerExpanded", false);
        private static readonly Type CompGasTrader = Type.GetType("GasNetwork.CompGasTrader,VanillaPowerExpanded", false);

        public static bool? IsCleanPowerPlant(Thing thing)
        {
            var cleanPower = thing.TryGetComp<CompCleanPower>();
            if (cleanPower != null)
            {
                if (cleanPower.Props.notActuallyAPowerPlant)
                {
                    return null;
                }

                return cleanPower.Props.clean;
            }

            if (CompGasTrader != null)
            {
                var comps = (thing as ThingWithComps)?.AllComps;

                if (comps != null)
                {
                    foreach (var comp in comps)
                    {
                        if (CompGasTrader.IsAssignableFrom(comp.GetType()))
                        {
                            return false;
                        }
                    }
                }
            }

            var powerTrader = thing.TryGetComp<CompPowerPlant>();
            if (powerTrader != null)
            {
                // subclasses include Steam, Wind, Solar, and Water.
                return powerTrader.GetType() != typeof(CompPowerPlant) && powerTrader.GetType() != CompGasPowerPlant;
            }

            return null;
        }

        public static bool? IsCleanPowerPlant(ThingDef thingDef)
        {
            var cleanPower = thingDef.GetCompProperties<CompProperties_CleanPower>();
            if (cleanPower != null)
            {
                if (cleanPower.notActuallyAPowerPlant)
                {
                    return null;
                }

                return cleanPower.clean;
            }

            if (CompGasTrader != null && thingDef.HasAssignableCompFrom(CompGasTrader))
            {
                return false;
            }

            var powerTrader = thingDef.CompDefForAssignableFrom<CompPowerPlant>();
            if (powerTrader != null)
            {
                return powerTrader.compClass != typeof(CompPowerPlant) && powerTrader.compClass != CompGasPowerPlant;
            }

            return null;
        }
    }
}
