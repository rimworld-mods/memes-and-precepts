﻿using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class IncidentWorker_EcologyPrimacy_Mechanoids : IncidentWorker
    {
        protected override bool CanFireNowSub(IncidentParms parms)
        {
            if (Faction.OfPlayerSilentFail?.ideos?.PrimaryIdeo?.HasMeme(MPDefOf.BenLubarMemesAndPrecepts_EcologyPrimacy) != true)
            {
                return false;
            }

            if (Faction.OfMechanoids == null)
            {
                return false;
            }

            return true;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            // TODO: a small cluster of mechanoids airdrop or walk in at map edge. mechanoids are only
            // of the types listed below. small ship parts can also drop in at random locations.
            // the ship parts are weaker and start expanding more slowly than full sized ship parts, but
            // accelerate in their expansion.
            //
            // mechanoid types:
            //
            // BUTCHER - basic slashing melee attack. 5x damage bonus against animals. killed animals instantly rot.
            // targets nearby wild animals over colonists, but can still attack humanoids and pets.
            //
            // STINKER - causes slow toxic buildup in a moderate area. tiny plant killing AOE.
            // constantly creates sewage at its current position if bad hygiene is installed.
            // constantly creates trash, vomit, slime, corpse bile, and ash at its current position.
            // explodes into a toxic cloud that causes toxic buildup on death in a smaller area.
            // ignores combat entirely. does not react to colonists or being attacked.
            //
            // BURNER - occasionally creates a small fire at its current position. constantly creates chemfuel,
            // napalm (rimefeller), oil (rimefeller), tar (alpha biomes) at its current position.
            // basic bludgeoning melee attack. explodes into fire when killed.
            Log.Error("Unimplemented incident " + this);
            return false;
        }
    }
}
