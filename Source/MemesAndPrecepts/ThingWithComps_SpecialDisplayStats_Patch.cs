﻿using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(ThingWithComps), nameof(ThingWithComps.SpecialDisplayStats))]
    static class ThingWithComps_SpecialDisplayStats_Patch
    {
        public static IEnumerable<StatDrawEntry> Postfix(IEnumerable<StatDrawEntry> __result, ThingWithComps __instance)
        {
            foreach (var entry in __result)
            {
                yield return entry;
            }

            var clean = CompCleanPower.IsCleanPowerPlant(__instance);
            if (clean.HasValue)
            {
                yield return new StatDrawEntry(
                    StatCategoryDefOf.Building,
                    "BenLubarMemesAndPrecepts_PowerPlantCleanliness".Translate(),
                    clean.Value ? "BenLubarMemesAndPrecepts_PowerPlantCleanliness_Clean".Translate() : "BenLubarMemesAndPrecepts_PowerPlantCleanliness_Dirty".Translate(),
                    clean.Value ? "BenLubarMemesAndPrecepts_PowerPlantCleanliness_Clean_Report".Translate() : "BenLubarMemesAndPrecepts_PowerPlantCleanliness_Dirty_Report".Translate(),
                    0
                );
            }
        }
    }
}
