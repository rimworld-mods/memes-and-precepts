﻿using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    [HarmonyPatch(typeof(LovePartnerRelationUtility), nameof(LovePartnerRelationUtility.GetPartnerInMyBed))]
    static class LovePartnerRelationUtility_GetPartnerInMyBed_Patch
    {
        public static bool Prefix(ref Pawn __result, Pawn pawn)
        {
            var bed = pawn.CurrentBed();
            if (bed == null)
            {
                return true;
            }

            if (bed.SleepingSlotsCount <= 1)
            {
                return true;
            }

            var partners = LovePartnerRelationUtility.ExistingLovePartners(pawn, allowDead: false).ToList();
            partners.Shuffle();

            if (BedUtility_WillingToShareBed_Patch.GetBedSharingPrecept(pawn.Ideo) == null)
            {
                bool anyBedSharingPrecept = false;
                foreach (var partner in partners)
                {
                    if (partner.def == PawnRelationDefOf.Lover || partner.def == PawnRelationDefOf.Fiance || partner.def == PawnRelationDefOf.Spouse)
                    {
                        if (BedUtility_WillingToShareBed_Patch.GetBedSharingPrecept(partner.otherPawn.Ideo) != null)
                        {
                            anyBedSharingPrecept = true;

                            break;
                        }
                    }
                }

                if (!anyBedSharingPrecept)
                {
                    return true;
                }
            }

            foreach (var partner in partners)
            {
                if (!LovePartnerRelationUtility.AreNearEachOther(pawn, partner.otherPawn))
                {
                    continue;
                }

                if (partner.otherPawn.CurJob?.playerForced == true || partner.otherPawn.CurrentBed()?.Medical == true || !partner.otherPawn.health.capacities.CanBeAwake)
                {
                    continue;
                }

                if (!new HistoryEvent(HistoryEventDefOf.GotLovin, pawn.Named(HistoryEventArgsNames.Doer), partner.otherPawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo() ||
                    !new HistoryEvent(HistoryEventDefOf.GotLovin, partner.otherPawn.Named(HistoryEventArgsNames.Doer), pawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo())
                {
                    continue;
                }

                if (partner.def == PawnRelationDefOf.Spouse)
                {
                    if (!new HistoryEvent(HistoryEventDefOf.GotLovin_Spouse, pawn.Named(HistoryEventArgsNames.Doer), partner.otherPawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo() ||
                        !new HistoryEvent(HistoryEventDefOf.GotLovin_Spouse, partner.otherPawn.Named(HistoryEventArgsNames.Doer), pawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo())
                    {
                        continue;
                    }
                }
                else
                {
                    if (!new HistoryEvent(HistoryEventDefOf.GotLovin_NonSpouse, pawn.Named(HistoryEventArgsNames.Doer), partner.otherPawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo() ||
                        !new HistoryEvent(HistoryEventDefOf.GotLovin_NonSpouse, partner.otherPawn.Named(HistoryEventArgsNames.Doer), pawn.Named(HistoryEventArgsNames.Victim)).DoerWillingToDo())
                    {
                        continue;
                    }
                }

                __result = partner.otherPawn;
                return false;
            }

            __result = null;
            return false;
        }
    }
}
