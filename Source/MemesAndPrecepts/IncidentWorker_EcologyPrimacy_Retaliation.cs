﻿using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace BenLubarMemesAndPrecepts
{
    public class IncidentWorker_EcologyPrimacy_Retaliation : IncidentWorker_RaidEnemy
    {
        public static IEnumerable<ThingDef> TerraformerMachineDefs
        {
            get
            {
                yield return MPDefOf.BenLubarMemesAndPrecepts_ClimateAdjuster;
                yield return MPDefOf.BenLubarMemesAndPrecepts_WeatherController;
                yield return MPDefOf.BenLubarMemesAndPrecepts_SunBlocker;
            }
        }

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            foreach (var def in TerraformerMachineDefs)
            {
                foreach (var map in Find.Maps)
                {
                    if (map.IsPlayerHome && map.listerBuildings.AllBuildingsColonistOfDef(def).Any())
                    {
                        return base.CanFireNowSub(parms);
                    }
                }
            }

            return false;
        }

        protected override string GetLetterText(IncidentParms parms, List<Pawn> pawns)
        {
            return base.GetLetterText(parms, pawns) + "\n\n" + def.letterText;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            if (parms.target is Map map && TerraformerMachineDefs.SelectMany(d => map.listerBuildings.AllBuildingsColonistOfDef(d)).TryRandomElement(out var target))
            {
                parms.attackTargets = new List<Thing> { target };

                return base.TryExecuteWorker(parms);
            }

            return false;
        }
    }
}
